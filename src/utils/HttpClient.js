import axios from "axios";
import join from "url-join";
import {
        apiUrl,

} from "../Constants";

const isAbsoluteURLRegex = /^(?:\w+:)\/\//;

axios.interceptors.request.use(async config => {

   
    if (!isAbsoluteURLRegex.test(config.url)) {
   
      config.url = join(apiUrl, config.url);
    }
    config.timeout = 10000; // 10 Second
    
    return config;
  });

  
  axios.interceptors.response.use(
    response => {
      return response;
    },
    error => {
 
      return error.response;
    }
  );
  
  export const httpClient = axios;