import {
  REPORT_FETCHING,
  REPORT_SUCCESS,
  REPORT_FAILED,
  LOGIN_STATUS,
  server,
} from "../Constants";
import { httpClient } from "../utils/HttpClient";
import axios from "axios";

export const setStateToSuccess = (payload) => ({
  type: REPORT_SUCCESS,
  payload,
});

const setStateToFetching = () => ({
  type: REPORT_FETCHING,
});

const setStateToFailed = () => ({
  type: REPORT_FAILED,
});

export const report = ({ startDate, endDate, select }) => {
  return async (dispatch) => {
    const Difference_In_Time = startDate.getTime() - endDate.getTime();
    const Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    const day_float = Math.abs(Difference_In_Days);
    const day = Math.round(day_float);

    const token = localStorage.getItem(LOGIN_STATUS);

    if (select == "Device") {
      const result = await httpClient.get(server.REPORT_DEVICE_URL, {
        headers: {
          Authorization: token,
        },
        params: {
          startDate: startDate,
          endDate: endDate,
          day: day,
        },
      });

      let { data } = result;
      if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
      } else {
        dispatch(setStateToFailed());
      }
    } else if (select == "Rack") {
      const result = await httpClient.get(server.REPORT_RACK_URL, {
        headers: {
          Authorization: token,
        },
        params: {
          startDate: startDate,
          endDate: endDate,
          day: day,
        },
      });

      let { data } = result;
      if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
      } else {
        dispatch(setStateToFailed());
      }
    } else if (select == "Group") {
      const result = await httpClient.get(server.REPORT_GROUP_URL, {
        headers: {
          Authorization: token,
        },
        params: {
          startDate: startDate,
          endDate: endDate,
          day: day,
        },
      });

      let { data } = result;
      if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
      } else {
        dispatch(setStateToFailed());
      }
    }
  };
};
