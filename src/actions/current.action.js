import {
    CURRENT_FETCHING,
    CURRENT_SUCCESS,
    CURRENT_FAILED,
    LOGIN_STATUS,
    server,
  } from "../Constants";
  import { httpClient } from "../utils/HttpClient";
  
  export const setStateToSuccess = (payload) => ({
    type: CURRENT_SUCCESS,
    payload,
  });
  
  const setStateToFetching = () => ({
    type: CURRENT_FETCHING,
  });
  
  const setStateToFailed = () => ({
    type: CURRENT_FAILED,
  });
  


  /*
      API รับ อลามทั้งหมด
  */
  export const getCurrent = () => {
    return (dispatch) => {
      dispatch(setStateToFetching());
      dogetCurrent(dispatch);
    };
  };
  
  export const dogetCurrent = async (dispatch) => {
    const token = localStorage.getItem(LOGIN_STATUS);
    let result = await httpClient.get(server.CURRENT_URL, {
      headers: {
        Authorization: token,
      },
    });
  
    let { data } = result;
    if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
    } else {
        dispatch(setStateToFailed());
    }
  };
  



