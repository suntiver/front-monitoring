import {
  ELECTRIC_FETCHING,
  ELECTRIC_SUCCESS,
  ELECTRIC_FAILED,
  LOGIN_STATUS,
  server,
} from "../Constants";
import { httpClient } from "../utils/HttpClient";

export const setStateToSuccess = (payload) => ({
  type: ELECTRIC_SUCCESS,
  payload,
});

const setStateToFetching = () => ({
  type: ELECTRIC_FETCHING,
});

const setStateToFailed = () => ({
  type: ELECTRIC_FAILED,
});

/*
    API รับค่ากระเเสไฟทั้งหมด
*/
export const getElectricCurrent = () => {
  return (dispatch) => {
    dispatch(setStateToFetching());
    dogetElectricCurrent(dispatch);
  };
};

export const dogetElectricCurrent = async (dispatch) => {
  const token = localStorage.getItem(LOGIN_STATUS);
  let result = await httpClient.get(server.ELECTRIC_CURRENT_URL, {
    headers: {
      Authorization: token,
    },
  });

  let { data } = result;
  console.log(data);
  if (data.status == true) {
      dispatch(setStateToSuccess(data.data));
  } else {
    dispatch(setStateToFailed());
  }
};
