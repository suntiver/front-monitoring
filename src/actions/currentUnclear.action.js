import {
    CURRENT_UNCLEAR_FETCHING,
    CURRENT_UNCLEAR_SUCCESS,
    CURRENT_UNCLEAR_FAILED,
    LOGIN_STATUS,
    server,
  } from "../Constants";
  import { httpClient } from "../utils/HttpClient";
  
  export const setStateToSuccess = (payload) => ({
    type: CURRENT_UNCLEAR_SUCCESS,
    payload,
  });
  
  const setStateToFetching = () => ({
    type: CURRENT_UNCLEAR_FETCHING,
  });
  
  const setStateToFailed = () => ({
    type: CURRENT_UNCLEAR_FAILED,
  });
  


 /*
      API รับ อลามที่ยังไม่เคลียร์ทั้งหมด
 */
 export const getCurrentUnClear = () => {
  return (dispatch) => {
        dispatch(setStateToFetching());
        dogetCurrentUnclear(dispatch);
  };
};




  export const dogetCurrentUnclear = async (dispatch) => {
    const token = localStorage.getItem(LOGIN_STATUS);
    let result = await httpClient.get(server.CURRENT_UNCLEAR_URL, {
      headers: {
        Authorization: token,
      },
    });
  
    let { data } = result;
    if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
    } else {
        dispatch(setStateToFailed());
    }
  };
  

