import {
  REGISTER_FETCHING,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
  REGISTER_SETVALUE,
  server,
} from "../Constants";
import { httpClient } from "./../utils/HttpClient";

export const setStateToFetching = () => ({
  type: REGISTER_FETCHING,
});

export const setStateToSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});

export const setStateToFailed = (payload) => ({
  type: REGISTER_FAILED,
  payload,
});

export const setStateToSETVALUE = (payload) => ({
  type: REGISTER_SETVALUE,
  payload,
});

export const register = ({ email, password, confirmpassword }) => {
  return async (dispatch) => {
    dispatch(setStateToFetching());

    if (password != confirmpassword) {
      dispatch(setStateToFailed("Incorrect  password."));
    } else {
      const result = await httpClient.post(server.REGISTER_URL, {
        email,
        password,
      });

      if (result.data.status) {
        dispatch(setStateToSuccess("ok"));
        dispatch(setStateToSETVALUE(true));
      } else {
        dispatch(setStateToFailed(result.data.message));
        dispatch(setStateToSETVALUE(false));
      }
    }
  };
};
