import {
    CHART_YESTERDAY_FETCHING,
    CHART_YESTERDAY_SUCCESS,
    CHART_YESTERDAY_FAILED,
    LOGIN_STATUS,
    server,
  } from "../Constants";
  import { httpClient } from "../utils/HttpClient";
  
  export const setStateToSuccess = (payload) => ({
    type: CHART_YESTERDAY_SUCCESS,
    payload,
  });
  
  const setStateToFetching = () => ({
    type: CHART_YESTERDAY_FETCHING,
  });
  
  const setStateToFailed = () => ({
    type: CHART_YESTERDAY_FAILED,
  });
  


  /*
      API รับ อลามทั้งหมด
  */
  export const getDataYesterday = () => {
    return (dispatch) => {
      dispatch(setStateToFetching());
      dogetDataYesterday(dispatch);
    };
  };
  
  export const dogetDataYesterday = async (dispatch) => {
    const token = localStorage.getItem(LOGIN_STATUS);
    let result = await httpClient.get(server.CHART_YESTERDAY, {
      headers: {
        Authorization: token,
      },
    });
  
    let { data } = result;
    if (data.status == true) {
        dispatch(setStateToSuccess(data.data));
    } else {
        dispatch(setStateToFailed());
    }
  };
  



