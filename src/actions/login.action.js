import {
  LOGIN_FETCHING,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_STATUS,
  LOGOUT,
  server
} from "../Constants";
import { httpClient } from "./../utils/HttpClient";

export const setStateToFetching = () => ({
  type: LOGIN_FETCHING,
});

export const setStateToSuccess = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});

export const setStateToFailed = (payload) => ({
  type: LOGIN_FAILED,
  payload,
});

export const setStateToLogout = () => ({
  type: LOGOUT
});



export const login = ({ email, password, history }) => {
  return async (dispatch) => {
    dispatch(setStateToFetching());
    
    const result = await httpClient.post(server.LOGIN_URL, {
      email,
      password,
    });

    if (result.data.status == true) {
      const token = result.data.token;
      localStorage.setItem(LOGIN_STATUS, token);
      dispatch(setStateToSuccess("success"));
      history.push("/dashboard");
    } else {
      dispatch(setStateToFailed(result.data.message));
    }
  };
};


export const reLogin = () => {
  return dispatch => {
 
  
    const loginStatus = localStorage.getItem(LOGIN_STATUS);
    if(loginStatus){
        dispatch(setStateToSuccess({}));
    }
  };
};


export const isLoggedIn = () => {
      const loginStatus = localStorage.getItem(LOGIN_STATUS);

      if(loginStatus){

          return true
      }else{
          return  false;
      }
 
};


export const logout = ({ history }) => {
  return dispatch => {
    localStorage.removeItem(LOGIN_STATUS);
    dispatch(setStateToLogout());
    // history.push("/");
  };
};