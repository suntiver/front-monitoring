import {
    CURRENT_FETCHING,
    CURRENT_SUCCESS,
    CURRENT_FAILED,

  } from "../Constants";
  
  const initialState = {
      result: null,
      isFetching: false,
      isError: false,
  };
  
  
  export default (state = initialState, { type, payload }) => {
      switch (type) {
        case   CURRENT_FETCHING:
          return { ...state, isFetching: true, isError: false, result: null };
        case   CURRENT_SUCCESS:
          return { ...state, result: payload, isFetching: false, isError: false };
        case CURRENT_FAILED:
          return { ...state, result: null, isFetching: false, isError: true };
        default:
          return state;
      }
    };