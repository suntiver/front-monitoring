import {
  LOGIN_FAILED,
  LOGIN_FETCHING,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./../Constants";

const initialState = {
  result: null,
  isFetching: false,
  error: false,
  approve:false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case LOGIN_FETCHING:
      return { ...state, isFetching: true, error: false, result: null ,approve:false};
    case LOGIN_FAILED:
      return { ...state, isFetching: false, error: true, result: payload ,approve:false};
    case LOGIN_SUCCESS:
      return { ...state, isFetching: false, error: false, result: payload ,approve:false};
    case LOGOUT:
      return initialState;
    default:
      return state;
  }
};
