import {
    CURRENT_UNCLEAR_FETCHING,
    CURRENT_UNCLEAR_SUCCESS,
    CURRENT_UNCLEAR_FAILED

  } from "../Constants";
  
  const initialState = {
      result: null,
      isFetching: false,
      isError: false,
  };
  
  
  export default (state = initialState, { type, payload }) => {
      switch (type) {
        case  CURRENT_UNCLEAR_FETCHING:
          return { ...state, isFetching: true, isError: false, result: null };
        case  CURRENT_UNCLEAR_SUCCESS:
          return { ...state, result: payload, isFetching: false, isError: false };
        case CURRENT_UNCLEAR_FAILED:
          return { ...state, result: null, isFetching: false, isError: true };
        default:
          return state;
      }
    };