import {
    CHART_YESTERDAY_FETCHING,
    CHART_YESTERDAY_SUCCESS,
    CHART_YESTERDAY_FAILED,

  } from "../Constants";
  
  const initialState = {
      result: null,
      isFetching: false,
      isError: false,
  };
  
  
  export default (state = initialState, { type, payload }) => {
      switch (type) {
        case   CHART_YESTERDAY_FETCHING:
          return { ...state, isFetching: true, isError: false, result: null };
        case   CHART_YESTERDAY_SUCCESS:
          return { ...state, result: payload, isFetching: false, isError: false };
        case  CHART_YESTERDAY_FAILED:
          return { ...state, result: null, isFetching: false, isError: true };
        default:
          return state;
      }
    };