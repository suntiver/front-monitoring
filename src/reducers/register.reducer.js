import {

    REGISTER_FETCHING,
    REGISTER_FAILED,
    REGISTER_SUCCESS,
    REGISTER_SETVALUE


} from "../Constants";

const initialState = {
    isFetching: false,
    result: null,
    error: false,
    status:false,
  };

  export default (state = initialState, { type, payload }) => {
    switch (type) {

      case REGISTER_FETCHING:
        return { ...state, isFetching: true, error: false, result: null };
      case REGISTER_FAILED:
        return { ...state, isFetching: false, error: true, result: payload };
      case REGISTER_SUCCESS:
        return { ...state, isFetching: false, error: false, result: payload };
      case  REGISTER_SETVALUE:
        return { ...state, status:payload};
      default:
        return state;
    }
  };






