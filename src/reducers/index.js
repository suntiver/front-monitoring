import { combineReducers } from "redux";
import registerReducer from "./register.reducer";
import loginReducer from "./login.reducer";
import electricReducer from "./electric.reducer";
import currentReducer from "./current.reducer";
import reportReducer from "./report.reducer";
import  currentUnclearReducer from "./currentUnclear.reducer";
import  chartYesterdayReducer from "./chartYesterday.reducer"; 

export default combineReducers({
      registerReducer,
      loginReducer,
      electricReducer,
      currentReducer,
      reportReducer,
      currentUnclearReducer,
      chartYesterdayReducer
});
