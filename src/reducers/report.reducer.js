import {
    REPORT_FETCHING,
    REPORT_SUCCESS,
    REPORT_FAILED,

  } from "../Constants";
  
  const initialState = {
      result: null,
      isFetching: false,
      isError: false,
  };
  
  
  export default (state = initialState, { type, payload }) => {
      switch (type) {
        case   REPORT_FETCHING:
          return { ...state, isFetching: true, isError: false, result: null };
        case   REPORT_SUCCESS:
          return { ...state, result: payload, isFetching: false, isError: false };
        case REPORT_FAILED:
          return { ...state, result: null, isFetching: false, isError: true };
        default:
          return state;
      }
    };