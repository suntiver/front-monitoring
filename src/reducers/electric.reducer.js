import {
  ELECTRIC_FETCHING,
  ELECTRIC_SUCCESS,
  ELECTRIC_FAILED,
} from "../Constants";

const initialState = {
    result: null,
    isFetching: false,
    isError: false,
};


export default (state = initialState, { type, payload }) => {
    switch (type) {
      case   ELECTRIC_FETCHING:
        return { ...state, isFetching: true, isError: false, result: null };
      case   ELECTRIC_SUCCESS:
        return { ...state, result: payload, isFetching: false, isError: false };
      case ELECTRIC_FAILED:
        return { ...state, result: null, isFetching: false, isError: true };
      default:
        return state;
    }
  };