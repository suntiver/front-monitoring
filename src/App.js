import React from "react";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";



/*Pages*/
import Login from "./components/pages/Login";
import Register from "./components/pages/Register";
import GenericNotFound from "./components/pages/GenericNotFound";
import Dashboard from "./components/pages/Dashboard";
import ElectricCurrent from "./components/pages/ElectricCurrent";
import CurrentAlarm from "./components/pages/CurrentAlarm";
import Report from "./components/pages/Report";

/*Fragments*/
import Header from "./components/fragments/Header";
import Menu from "./components/fragments/Menu";
import * as loginActions from "./actions/login.action";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#135ab8",
    },
    secondary: {
      main: "#e85f5f",
    },
  },
});

const useStyles = makeStyles((theme) => ({
 
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },

}));



const SecuredRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      // ternary condition
      loginActions.isLoggedIn() ? (
        <Component {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      // ternary condition
      loginActions.isLoggedIn() ? (
        <Redirect to="/dashboard" />
      ) : (
        <Login {...props} />
      )
    }
  />
);

export default function App() {
  const classes = useStyles();
  const [openDrawer, setOpenDrawer] = React.useState(false);
  const dispatch = useDispatch();

  const handleDrawerOpen = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  React.useEffect(() => {
    
    dispatch(loginActions.reLogin());
  }, [dispatch]);

  const loginReducer = useSelector(({ loginReducer }) => loginReducer);

  return (
    <div>
      <ThemeProvider theme={theme}>
        <Router>
          {loginReducer.result && !loginReducer.error && (
            <Header handleDrawerOpen={handleDrawerOpen} open={openDrawer} />
          )}
          {loginReducer.result && !loginReducer.error && (
            <Menu open={openDrawer} handleDrawerClose={handleDrawerClose} />
          )}

         <main   className={classes.content} >
                  <Container style={{ marginTop: '5%' }}>
                    <Switch>
                      <LoginRoute path="/login" component={Login}   />
                      <Route path="/register" component={Register} />
                      <SecuredRoute path="/dashboard" component={Dashboard} />
                      <SecuredRoute path="/electriccurrent" component={ElectricCurrent} />
                      <SecuredRoute path="/currentalarm" component={CurrentAlarm} />
                      <SecuredRoute path="/report" component={Report} />
                      <Route
                        exact={true}
                        path="/"
                        component={() => <Redirect to="/login" />}
                      />
                      <Route component={GenericNotFound} />
                    </Switch>
                  </Container>
          </main>
        </Router>
      </ThemeProvider>
    </div>
  );
}
