import React from "react";
import { NavLink } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Home from "@material-ui/icons/Home";
import Assessment from "@material-ui/icons/Assessment";
import Notifications from "@material-ui/icons/Notifications";
import BarChart from "@material-ui/icons/BarChart";
const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
});

export default function Menu(props) {
  const classes = useStyles();

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
    >
      <List>
        <ListItem
          component={NavLink}
          to="/dashboard"
          button
          key="dashboard"
          // activeClassName={classes.isActive}
        >
          <ListItemIcon>
            <Home />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>

        <ListItem
          component={NavLink}
          to="/electriccurrent"
          button
          key="electriccurrent"
          // activeClassName={classes.isActive}
        >
          <ListItemIcon>
            <Assessment />
          </ListItemIcon>
          <ListItemText primary="Electric current" />
        </ListItem>

        <ListItem
          component={NavLink}
          to="/currentalarm"
          button
          key="currentalarm"
          // activeClassName={classes.isActive}
        >
          <ListItemIcon>
            <Notifications />
          </ListItemIcon>
          <ListItemText primary="Current Alarm" />
        </ListItem>

        <ListItem
          component={NavLink}
          to="/report"
          button
          key="report"
          // activeClassName={classes.isActive}
        >
          <ListItemIcon>
            <BarChart />
          </ListItemIcon>
          <ListItemText primary="Report" />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <React.Fragment key={"left"}>
        <Drawer
          anchor={"left"}
          open={props.open}
          onClose={props.handleDrawerClose}
        >
          {list("left")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
