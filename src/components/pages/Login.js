import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Formik } from "formik";
import {
  Card,
  CardContent,
  CardMedia,
  Button,
  Typography,
  TextField,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import Alert from "@material-ui/lab/Alert";
import { Container } from "@material-ui/core";
import * as registerActions from "./../../actions/register.action";
import * as loginActions from "./../../actions/login.action";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 120,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login(props) {
  const classes = useStyles();

  const dispatch = useDispatch();
  const loginReducer = useSelector(({ loginReducer }) => loginReducer);
   




  function returnRegister() {
    props.history.push("/register");
    dispatch(registerActions.setStateToSETVALUE(false));
  }

  function showForm({ values, handleChange, handleSubmit }) {
    return (
      <form className={classes.form} onSubmit={handleSubmit}>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          onChange={handleChange}
          id="email"
          label="email"
          autoComplete="email"
          type="email"
          value={values.email}
          autoFocus
        />

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          onChange={handleChange}
          name="password"
          label="Password"
          type="password"
          id="password"
          value={values.password}
          autoComplete="current-password"
        />

        {loginReducer.error && (
          <Alert severity="error">{loginReducer.result}</Alert>
        )}

        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Login
        </Button>
        <Button
          fullWidth
          size="small"
          color="primary"
          onClick={() => returnRegister()}
        >
          Register
        </Button>
      </form>
    );
  }



  return (
    <Container style={{ display: "flex",justifyContent: "center" ,marginTop: '5%' }}>
    <div style={{ margin: 0, position: "absolute", top: "10%" }}>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={`${process.env.PUBLIC_URL}/images/authen_header.png`}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Login
          </Typography>

          <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values) => {
                           
              dispatch(loginActions.login({ ...values, ...props }));
             

            }}
          >
            {(props) => showForm(props)}
          </Formik>
        </CardContent>
      </Card>
    </div>
    </Container>
  );
}
