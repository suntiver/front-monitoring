import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";
import Moment from "react-moment";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { useDispatch, useSelector } from "react-redux";
import * as electricActions from "./../../actions/electric.action";
import Paper from "@material-ui/core/Paper";
import Chip from "@material-ui/core/Chip";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import moment               from 'moment';
import SwapVerticalCircle from "@material-ui/icons/SwapVerticalCircle";




const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

export default function ElectricCurrent(props) {
  const dispatch = useDispatch();
  const electricReducer = useSelector(({ electricReducer }) => electricReducer);

  React.useEffect(() => {
    dispatch(electricActions.getElectricCurrent());
  }, [dispatch]);

  const columns = [
    {
      title: "Datetime",
      cellStyle: { minWidth: 250 },
      render: (item) => (
        <Typography>
            {/* <span className="number">
                   {moment(item.datetime).format('YYYY/MM/DD-hh:mm:ss')}
            </span> */}
             <span>{(item.datetime)}</span>
        </Typography>
      ),
    },
    
    {
      title: "Device-name",
      field: "nameDevice",
   
    },

    {
      title: "Breaker-name",
      field: "nameBreaker",
   
    },


    {
      title: "Rack-name",
      field: "nameRack",
    },
    {
      title: "Group-name",
      field: "nameGroup",
    },
    {
      title: "Current",
      render: (item) => (
        <Typography variant="body1" >
          {item.status_current == "Close" && (
          
            <Chip className={classes.chip} color="inherit" label="None" />
        

          )}
          {item.status_current == "Open" &&
            item.current > item.settingLow &&
            item.current < item.settingHigh && (
              <Chip
                className={classes.chip}
                style={{ background: "#76ff03" }}
                label={item.current}
              />
            )}

          {item.status_current == "Open" && item.current <= item.settingLow && (
            <Chip
              className={classes.chip}
              style={{ background: "#ffea00" }}
              label={item.current}
            />
          )}

          {item.status_current == "Open" &&
            item.current >= item.settingHigh && (
              <Chip
                className={classes.chip}
                color="secondary"
                label={item.current}
              />
            )}
        </Typography>
      ),
    }
  ];

  const useStyles = makeStyles((theme) => ({
    root: {
      width: "100%",
    },
    link: {
      display: "flex",
    },
    icon: {
      marginRight: theme.spacing(0.5),
      width: 20,
      height: 20,
    },
  }));

  const classes = useStyles();
  const data = electricReducer.result ? electricReducer.result : [];
  return (
    <div className={classes.root}>
      <Breadcrumbs aria-label="breadcrumb" style={{marginBottom:'2%'}} >
        <Typography color="textPrimary" className={classes.link}>
          <SwapVerticalCircle className={classes.icon} style={{color:'#e85f5f'}} />
          High current
        
        </Typography>
        <Typography color="textPrimary" className={classes.link}>
          <SwapVerticalCircle className={classes.icon}   style={{color:'#ffea00'}}  />
          Low current
        </Typography>
        <Typography color="textPrimary" className={classes.link}>
          <SwapVerticalCircle className={classes.icon}  style={{color:'#76ff03'}} />
          Normal current
        </Typography>
        <Typography color="textPrimary" className={classes.link}>
          <SwapVerticalCircle className={classes.icon}  style={{color:'#E0E0E0'}}/>
          Device fail
        </Typography>
      
      </Breadcrumbs>

      <MaterialTable
        options={{
          pageSize: 8,
          search: true,
          rowStyle: (rowData, index) => ({
            alignItems: "center",
            backgroundColor: index % 2 == 0 ? "#f8faf9" : "#fff",
          }),
        }}
        icons={tableIcons}
        columns={columns}
        data={electricReducer.result ? electricReducer.result : []}
        title={"Electric current all"}
        components={{
          Container: (props) => <Paper {...props} elevation={10} />,
          Toolbar: (props) => (
            <div>
              <MTableToolbar {...props} />
            </div>
          ),
        }}
      />
    </div>
  );
}
