import React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as registerActions from "./../../actions/register.action";


/*From */
import { makeStyles } from "@material-ui/core/styles";
import { Formik } from "formik";
import {
  Card,
  CardContent,
  CardMedia,
  Button,
  Typography,
  TextField,
} from "@material-ui/core";

/*Dialog box */
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";


/*Alert notification*/
import Alert from "@material-ui/lab/Alert";
import { Container } from "@material-ui/core";




const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 120,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Register(props) {
  const classes = useStyles();

  const dispatch = useDispatch();

  const registerReducer = useSelector(({ registerReducer }) => registerReducer);

  const [showDialog, setShowDialog] = React.useState(false);


  React.useEffect(() => {
    if (registerReducer.status) {
      setShowDialog(true);
    } else {
      setShowDialog(false);
    }
  });

  function resetvalue() {
    dispatch(registerActions.setStateToFetching());
    props.history.push("/login");
  }



  function showForm({
    values,
    handleChange,
    handleSubmit,
    setFieldValue,
    isSubmitting,
  }) {
    return (
      <form className={classes.form} onSubmit={handleSubmit}>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          onChange={handleChange}
          id="email"
          label="email"
          autoComplete="email"
          type="email"
          value={values.email}
          autoFocus
        />

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          onChange={handleChange}
          name="password"
          label="Password"
          type="password"
          id="password"
          value={values.password}
          autoComplete="current-password"
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          onChange={handleChange}
          name="confirmpassword"
          label="confirmpassword"
          type="password"
          id="confirmpassword"
          value={values.confirmpassword}
        />

     
     

          {registerReducer.error && (
            <Alert severity="error">{registerReducer.result}</Alert>
          )}



        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Register
        </Button>
        <Button
          onClick={() => props.history.push("/login")}
          fullWidth
          size="small"
          color="primary"
          onClick={() => resetvalue()}
        >
          Cancel
        </Button>
      </form>
    );
  }

  return (
    <Container style={{ display: "flex",justifyContent: "center" ,marginTop: '5%' }}>
    <div style={{ margin: 0, position: "absolute", top: "10%" }}>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={`${process.env.PUBLIC_URL}/images/authen_header.png`}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Register
          </Typography>

          <Formik
            initialValues={{ email: "", password: "", confirmpassword: "" }}
            onSubmit={(value) => {
              dispatch(registerActions.register({ ...value }));
            }}
          >
            {(props) => showForm(props)}
          </Formik>
        </CardContent>
      </Card>

      <Dialog
        open={showDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Register success?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending
            anonymous location data to Google, even when no apps are running.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            color="primary"
            autoFocus
            onClick={() => {
              props.history.push("/login");
            }}
          >
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
     </Container>
  );
}
