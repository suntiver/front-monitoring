import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";

import { useDispatch, useSelector } from "react-redux";
import * as chartYesterdaytActions from "./../../actions/chartYesterday.action";
export default function Dashboard() {
  
 
  const dispatch = useDispatch();

  const chartYesterdayReducer = useSelector(({  chartYesterdayReducer }) =>  chartYesterdayReducer);

  React.useEffect(() => {
    dispatch(chartYesterdaytActions.getDataYesterday());
    }, [dispatch]);



  

  return (
    <div style={{ marginTop: "7%" }}>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Card>
            <CardContent wrap="nowrap">
              <Grid container spacing={3}>
                <Grid item xs={9}>
                  <Typography
                    variant="h5"
                    component="h2"
                    style={{ textAlign: "center" }}
                  >
                    1245.42 kWh
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    style={{ textAlign: "center" }}
                  >
                    Today
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Avatar>W</Avatar>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card>
            <CardContent wrap="nowrap">
              <Grid container spacing={3}>
                <Grid item xs={9}>
                  <Typography
                    variant="h5"
                    component="h2"
                    style={{ textAlign: "center" }}
                  >
                    1245.42 kWh
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    style={{ textAlign: "center" }}
                  >
                    Yesterday
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Avatar>W</Avatar>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card>
            <CardContent wrap="nowrap">
              <Grid container spacing={3}>
                <Grid item xs={9}>
                  <Typography
                    variant="h5"
                    component="h2"
                    style={{ textAlign: "center" }}
                  >
                    -12 %
                  </Typography>

                  <Typography
                    variant="body2"
                    component="p"
                    style={{ textAlign: "center" }}
                  >
                    Comparetion
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Avatar>W</Avatar>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <div>
        <h3 style={{alignContent:'center'}}>หน้านี้ยังเป็นเพียงเเค่หน้าจำลอง ข้อมูลที่เเสดงจะยังไม่ใช่ข้อมูลจริง</h3>
      </div>
 
  
    </div>
  );
}
