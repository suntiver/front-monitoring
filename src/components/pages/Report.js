import React from "react";
import { Formik } from "formik";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";
import * as reportActions from "./../../actions/report.action";
import MaterialTable, { MTableToolbar } from "material-table";
import { forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";
import Moment from "react-moment";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

import Paper from "@material-ui/core/Paper";
import Chip from "@material-ui/core/Chip";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import SwapVerticalCircle from "@material-ui/icons/SwapVerticalCircle";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
  },
  selett: {
    marginTop: "5%",
  },
  marginButton: {
    marginTop: "8%",
  },
}));

export default function Report() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const reportReducer = useSelector(({ reportReducer }) => reportReducer);
  
  const [startDate, setSelectedstartDate] = React.useState(new Date());
  const [endDate, setSelectedendDate] = React.useState(new Date());
  const [select, setSelect] = React.useState("");


  const handlestartDateChange = (date) => {
    setSelectedstartDate(date);
  };
  const handleendDateChange = (date) => {
    setSelectedendDate(date);
  };
  const handleselectChange = (event) => {
    setSelect(event.target.value);
  };


  const columns = [
   
    {
      title: "Devices-name",
      field: "nameDevice",
      cellStyle: { minWidth: 170 },
   
    },
    {
      title: "Rack-name",
      field: "nameRack",
      cellStyle: { minWidth: 170 },
   
    },
    {
      title: "Group-name",
      field: "nameGroup",
      cellStyle: { minWidth: 170 },
   
    },
    {
      title: "Contract (Amp)",
      field: "contract",
   
    },
    {
      title: "current (Amp)",
      field: "current",
   
    },
    {
      title: "Avg Watt ",
      field: "w",
   
    },
    {
      title: "Avg kW",
      field: "kw",
    },
    {
      title: "Avg kWh",
      field: "kwh",
    },
    {
      title: "Max (Amp)",
      field: "max",
    },
    {
      title: "Min (Amp)",
      field: "min",
    },
   
  ];

  function showForm({ values, handleChange, handleSubmit }) {
    return (
      <form
        onSubmit={handleSubmit}
        variant="outlined"
        justifyContent="flex-start"
      >
        <Paper className={classes.paper}>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <FormControl fullWidth>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="startDate"
                    label="Start date"
                    format="MM/dd/yyyy"
                    onChange={handlestartDateChange}
                    value={startDate}
                    required
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <FormControl fullWidth>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="endDate"
                    label="end Date"
                    format="MM/dd/yyyy"
                    onChange={handleendDateChange}
                    value={endDate}
                    required
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
            </Grid>

            <Grid item xs={3}>
              <FormControl fullWidth className={classes.selett}>
                <InputLabel id="demo-simple-select-label">Select</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="selectSubject"
                  required
                  onChange={handleselectChange}
                >
                  <MenuItem value={"Device"}>Device</MenuItem>
                  <MenuItem value={"Rack"}>Rack</MenuItem>
                  <MenuItem value={"Group"}>Group</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <Button
                variant="contained"
                size="large"
                fullWidth
                color="primary"
                className={classes.marginButton}
                type="submit"
              >
                Search
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </form>
    );
  }

  return (
    <div className={classes.root}>
      <Formik
        initialValues={{ startDate: "", endDate: "", selectSubject: "" }}
        onSubmit={() => {
          dispatch(reportActions.report({ startDate, endDate, select }));
        }}
      >
        {(props) => showForm(props)}
      </Formik>

      <MaterialTable
        style={{marginTop:"3%"}}
        options={{
          exportButton: true,
          pageSize: 8,
          search: true,
          rowStyle: (rowData, index) => ({
            alignItems: "center",
            backgroundColor: index % 2 == 0 ? "#f8faf9" : "#fff",
          }),
        }}
        icons={tableIcons}
        columns={columns}
        data={reportReducer.result ? reportReducer.result : []}
        title={"Electric current all"}
        components={{
          Container: (props) => <Paper {...props} elevation={10} />,
          Toolbar: (props) => (
            <div>
              <MTableToolbar {...props} />
            </div>
          ),
        }}
      />


    </div>
  );
}
