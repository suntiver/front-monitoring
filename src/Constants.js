/*
    Login
*/
export const LOGIN_FETCHING = "LOGIN_FETCHING";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_STATUS = "LOGIN_STATUS";
export const LOGOUT = "LOGOUT";


/*
    Register
*/
export const REGISTER_FETCHING = "REGISTER_FETCHING";
export const REGISTER_FAILED = "REGISTER_FAILED";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_SETVALUE = "REGISTER_SETVALUE";


/*
    Page Rlectriccurrent
*/
export const ELECTRIC_FETCHING = "ELECTRIC_CURRENT_FETCHING";
export const ELECTRIC_SUCCESS = "ELECTRIC_CURRENT_SUCCESS";
export const ELECTRIC_FAILED = "ELECTRIC_CURRENT_FAILED";
export const ELECTRIC_CLEAR = "ELECTRIC_CURRENT_CLEAR";

/*
    Page Current
*/
export const  CURRENT_FETCHING = "CURRENT_FETCHING";
export const  CURRENT_SUCCESS = "CURRENT_SUCCESS";
export const  CURRENT_FAILED = "CURRENT_FAILED";
export const  CURRENT_CLEAR = "CURRENT_CLEAR";


/*
    Chart js Yesterday
*/
export const  CHART_YESTERDAY_FETCHING = "CHART_YESTERDAY_FETCHING";
export const  CHART_YESTERDAY_SUCCESS = "CHART_YESTERDAY_SUCCESS";
export const  CHART_YESTERDAY_FAILED = "CHART_YESTERDAY_FAILED";
export const  CHART_YESTERDAY_CLEAR = "CHART_YESTERDAY_CLEAR";


/*
    Page REPORT
*/
export const  REPORT_FETCHING = "REPORT_FETCHING";
export const  REPORT_SUCCESS = "REPORT_SUCCESS";
export const  REPORT_FAILED = "REPORT_FAILED";
export const  REPORT_CLEAR = "REPORT_CLEAR";


/*
    CURRENT_UNCLEAR
*/
export const  CURRENT_UNCLEAR_FETCHING = "CURRENT_UNCLEAR_FETCHING";
export const  CURRENT_UNCLEAR_SUCCESS = "CURRENT_UNCLEAR_SUCCESS";
export const  CURRENT_UNCLEAR_FAILED = "CURRENT_UNCLEAR_FAILED";
export const  CURRENT_UNCLEAR_CLEAR = "CURRENT_UNCLEAR_CLEAR";



/*
    Notification
*/
export const NOTIFICATION = "http://172.16.12.40:9000/";

/*
    Call API
*/

export const apiUrl = "http://172.16.12.40:4000/api";
export const server = {
 
        REGISTER_URL: `/register`,
        LOGIN_URL:`/login-user`,
        ELECTRIC_CURRENT_URL:`/electric-current`,
        CURRENT_URL :`/current-all`,
        CURRENT_UNCLEAR_URL :`/alarm-unclear`,
        REPORT_DEVICE_URL:`/report-device`,
        REPORT_RACK_URL:`/report-rack`,
        REPORT_GROUP_URL:`/report-group`,
        CHART_YESTERDAY:`/current-yesterday`

};


/*
    Error Code
*/
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";
export const NETWORK_CONNECTION_MESSAGE ="Cannot connect to server, Please try again.";
